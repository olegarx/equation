#ifndef EQUATION_H
#define EQUATION_H

#include "SolveResult.h"

class Equation {
public:
	Equation(double a, double b, double c);

	SolveResult solve() const;
private:
	double a;
	double b;
	double c;
};

#endif