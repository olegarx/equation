#include "Equation.h"
#include <cmath>

Equation::Equation(double a, double b, double c) {
	this->a = a;
	this->b = b;
	this->c = c;
}

SolveResult Equation::solve() const {
	if (a != 0) {
		double discriminant = b*b - 4 * a*c;
		double x1 = (-b + std::sqrt(discriminant)) / (2 * a);
		double x2 = (-b - std::sqrt(discriminant)) / (2 * a);
		return SolveResult(x1, x2);
	}
	
	double x = -c / b;
	return SolveResult(x, x);
}