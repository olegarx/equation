#include "SolveResult.h"

SolveResult::SolveResult(double firstRadical, double secondRadical) {
	this->firstRadical = firstRadical;
	this->secondRadical = secondRadical;
}

double SolveResult::getFirstRadical() const {
	return firstRadical;
}

double SolveResult::getSecondRadical() const {
	return secondRadical;
}