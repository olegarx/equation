#ifndef SOLVERESULT_H
#define SOLVERESULT_H

class SolveResult {
public:
	SolveResult(double firstRadical, double secondRadical);

	double getFirstRadical() const;
	double getSecondRadical() const;
private:
	double firstRadical;
	double secondRadical;
};

#endif