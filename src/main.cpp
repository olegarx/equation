#include <iostream>
#include "equation/Equation.h"

int main() {
	Equation firstEquation(1, 3, 2);
	Equation secondEquation(0, 3, 6);

	SolveResult radicalsFirstEquation = firstEquation.solve();
	SolveResult radicalsSecondEquation = secondEquation.solve();

	std::cout << "Radicals a*x*x + b*x + c = 0: " << std::endl;
	std::cout << radicalsFirstEquation.getFirstRadical() << " " << radicalsFirstEquation.getSecondRadical() << std::endl;

	std::cout << "Radicals b*x + c = 0: " << std::endl;
	std::cout << radicalsSecondEquation.getFirstRadical() << " " << radicalsSecondEquation.getSecondRadical() << std::endl;

	return 0;
}